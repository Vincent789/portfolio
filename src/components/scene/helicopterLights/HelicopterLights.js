import { useRef, useEffect } from "react"
import { useControls} from "../../../store"
import gsap from 'gsap'
import Helicopter from "../helicopter/Helicopter"

function PointLight(props){
    return (
        <group {...props} dispose={null}>
            <pointLight color={props.type === "back" ? "red" : "yellow"} intensity={props.intensity} distance={2}/>
        </group>
    )
}

function HelicopterLights(props){
    const HelicopterLight = useRef()

    const key = useControls((state) => state.keyPressed)

    /* useEffect(() => {
        if (key === "right"){
            gsap.to(frontLights.current.position, {x: 2, duration: carLateralSlideSpeed})
            gsap.to(backLights.current.position, {x: 2, duration: carLateralSlideSpeed})
        }
        if (key === "left"){
            gsap.to(frontLights.current.position, {x: 0, duration: carLateralSlideSpeed})
            gsap.to(backLights.current.position, {x: 0, duration: carLateralSlideSpeed})
        }
        if (key === "up"){
            gsap.to(frontLights.current.position, {z: -1, duration: carFrontSlideSpeed})
            gsap.to(backLights.current.position, {z: -1, duration: carFrontSlideSpeed})
        }
        if (key === "down"){
            gsap.to(frontLights.current.position, {z: 0, duration: carBreakingSlideSpeed})
        }
    }) */

    return (
        <group {...props}>
            <group ref={HelicopterLight}>
                <PointLight type="front" intensity={20} distance={2}/>
            </group>
        </group>
    )
}

export default HelicopterLights